TARGET = HelloWorld
OBJS = main.c

CFLAGS = -02 -G0 -Wall
CXXFLAGS = $(CFLAGS) -fno-exceptions -fno-rtti
ASFLAGS = $(CFLAGS)

EXTRA_TARGETS = EBOOT.PBP
PSP_EBOOT_TITLE = Hello World

PSPSDK=$(shell psp-config --pspsdk-patch)
 include $ (PSPSDK)/lib/build.mak 